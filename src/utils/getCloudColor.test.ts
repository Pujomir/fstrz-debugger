import getCloudColor from './getCloudColor';
import { DebugData } from '../Declarations';

it('should return red if not plugged', function () {
    expect(getCloudColor({
        statusCode: 200,
        plugged: false
    })).toBe('red');
})

it('should return orange if plugged but not optimised', function () {
    expect(getCloudColor({
        statusCode: 200,
        plugged: true,
        fstrzFlags: []
    })).toBe('orange');
})

it('should return orange if plugged and optimised', function () {
    expect(getCloudColor({
        statusCode: 200,
        plugged: true,
        fstrzFlags: ['optimisée']
    })).toBe('#6AB420');
})