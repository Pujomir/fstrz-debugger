import { DebugData } from '../Declarations';

const getCloudColor = (debugData: DebugData): string => {
    const colors = {
        green: '#6AB420',
        orange: 'orange',
        red: 'red'
    };

    if (debugData.plugged &&
        debugData.fstrzFlags?.includes('optimisée'))
        return colors['green'];
    if (debugData.plugged)
        return colors['orange'];
    return colors['red'];
}

export default getCloudColor;