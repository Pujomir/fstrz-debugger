import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders fasterize debugger', () => {
  render(<App />);
  const linkElement = screen.getByText(/FASTERIZE DEBUGGER/i);
  expect(linkElement).toBeInTheDocument();
});
