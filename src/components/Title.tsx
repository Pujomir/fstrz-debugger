import React from 'react';
import './Title.css'

const Title: React.FC = ({children}) => {
    return (
        <div className="Title">
            <span className="TitleRectangle" />
            {children}
        </div>
    );
}

export default Title;