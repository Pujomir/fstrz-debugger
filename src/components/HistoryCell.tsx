import React from 'react';
import dayjs from 'dayjs';
import { FaCloud } from 'react-icons/fa';
import { HistoryEntry } from '../Declarations';
import displayFlag from '../utils/displayFlag';
import getCloudColor from '../utils/getCloudColor';
import './HistoryCell.css'

type HistoryCellProps = {
    historyEntry: HistoryEntry
};

const HistoryCell: React.FC<HistoryCellProps> = ({historyEntry}) => {
    return (
        <tr className="HistoryCell">
            <td>{dayjs(historyEntry.time).format('DD/MM/YYYY')}</td>
            <td><strong>{historyEntry.url}</strong></td>
            <td><FaCloud color={getCloudColor(historyEntry.debugData)} size="2rem" /></td>
            <td>
                {historyEntry.debugData.fstrzFlags &&
                historyEntry.debugData.fstrzFlags.map((flag, index) => (
                    <span key={index} className="fstrzFlag">{displayFlag(flag)}</span>
                ))}
            </td>
            <td>{historyEntry.debugData.cloudfrontStatus ?
                <span className="cloudfrontStatus">{historyEntry.debugData.cloudfrontStatus}</span>
                : null
            }</td>
            <td>{historyEntry.debugData.cloudfrontPOP}</td>
        </tr>
    );
}

export default HistoryCell;