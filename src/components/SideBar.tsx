import React from 'react';
import './SideBar.css'

const SideBar: React.FC = ({children}) => {
    return (
        <div className="SideBar">
            <div className="SideBarContent">
                {children}
            </div>
        </div>
    );
}

export default SideBar;