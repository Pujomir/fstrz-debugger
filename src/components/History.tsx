import React from 'react';
import HistoryCell from './HistoryCell';
import { HistoryEntry } from '../Declarations';
import './History.css'

type HistoryProps = {
    historyEntries: HistoryEntry[]
};

const History: React.FC<HistoryProps> = ({ historyEntries }) => {
    return (
        <table className="History">
            <thead className="HistoryTitle">
                <tr>
                    <td>Date</td>
                    <td>URL</td>
                    <td>Status</td>
                    <td>Flags</td>
                    <td>Cloudfront status</td>
                    <td>Cloudfront pop</td>
                </tr>
            </thead>
            <tbody>
                {historyEntries.map((historyEntry: HistoryEntry, index: number) => (
                    <HistoryCell key={index} historyEntry={historyEntry} />
                ))}
            </tbody>
        </table>
    );
}

export default History;