export type DebugData = {
    statusCode: number,
    plugged: boolean,
    fstrzFlags?: string[],
    cloudfrontStatus?: string,
    cloudfrontPOP?: string
};

export type HistoryEntry = {
    url: string,
    debugData: DebugData,
    time: Date
}